package br.ufc.es.emufc.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.cielyang.android.clearableedittext.ClearableEditText;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import br.ufc.es.emufc.R;
import br.ufc.es.emufc.dao.EquipmentPrefs;
import br.ufc.es.emufc.dao.LocalPrefs;
import br.ufc.es.emufc.models.Equipment;
import br.ufc.es.emufc.utils.EquipmentsAdapter;

public class BuscaActivity extends Fragment {

    ClearableEditText etBusca;
    ListView listView;
    private Bundle savedState = null;
    final String BUSCA = "Busca";
    final String ET_BUSCA_SAVED = "etBusca";
    SharedPreferences settings;
    public static SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        settings = getActivity().getSharedPreferences("Busca", Context.MODE_PRIVATE);
        editor = settings.edit();

        View v = inflater.inflate(R.layout.activity_busca, container, false);
        etBusca = (ClearableEditText) v.findViewById(R.id.et_busca);
        listView = (ListView) v.findViewById(R.id.lv_buscaEquip);

        ImageSpan imageHint = new ImageSpan(getActivity().getApplicationContext(), R.drawable.lupa2);
        SpannableString spannableString = new SpannableString("_ Digite nome ou descrição do equipamento");
        spannableString.setSpan(imageHint, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        etBusca.setHint(spannableString);

        etBusca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setListEquipments();
            }
        });

        return v;
    }

    public List<Equipment> getAllEquipments(String busca) {
        EquipmentPrefs equipmentPrefs = new EquipmentPrefs(getActivity().getApplicationContext());
        List<Equipment> listEquip = equipmentPrefs.getAllEquipments();
        List<Equipment> listFilter = new ArrayList<>();

        for (int i = 0; i < listEquip.size(); i++) {
            if (removeAccent(listEquip.get(i).nome.toLowerCase()).contains(removeAccent(busca)) ||
                    removeAccent(listEquip.get(i).descricao.toLowerCase()).contains(removeAccent(busca))) {
                listFilter.add(listEquip.get(i));
            }
        }

        return listFilter;
    }

    private String removeAccent(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    private void setListEquipments() {
        EquipmentsAdapter equipmentsAdapter = new EquipmentsAdapter(getActivity().getApplicationContext(), R.layout.custom_list2, getAllEquipments(etBusca.getText().toString().trim()));
        listView.setAdapter(equipmentsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Equipment equip = (Equipment) adapterView.getAdapter().getItem(i);

                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Equipment", equip);
                intent.putExtras(bundle);
                intent.setClass(getActivity(), EquipmentActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        saveData();
    }

    @Override
    public void onResume() {
        super.onResume();
        readData();
    }

    public void readData() {
        etBusca.setText(settings.getString("etBusca", ""));
        setListEquipments();
    }

    public void saveData() {
        editor.putString("etBusca", etBusca.getText().toString());
        editor.commit();
    }
}

