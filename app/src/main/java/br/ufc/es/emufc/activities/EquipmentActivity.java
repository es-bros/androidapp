package br.ufc.es.emufc.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;

import br.ufc.es.emufc.R;
import br.ufc.es.emufc.dao.EquipmentPrefs;
import br.ufc.es.emufc.dao.LocalPrefs;
import br.ufc.es.emufc.models.Equipment;
import br.ufc.es.emufc.models.Local;
import br.ufc.es.emufc.models.Responsible;
import br.ufc.es.emufc.utils.ImageStorage;

public class EquipmentActivity extends AppCompatActivity {

    TextView tvNome;
    TextView tvDescricao;
    TextView tvNomeResp;
    TextView tvEmailResp;
    TextView tvTelResp;
    TextView tvLocal;
    Equipment equipment;
    LinearLayout linearLayoutRota;
    LinearLayout linearLayoutFeedback;
    double latitude = 0.0;
    double longitude = 0.0;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    EquipmentPrefs equipmentPrefs;
    LocalPrefs localPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        ImageView toolbarImage = (ImageView) findViewById(R.id.imageToolbar);

        tvNome = (TextView) findViewById(R.id.tv_toolbar);
        tvDescricao = (TextView) findViewById(R.id.tv_descEquip);
        tvNomeResp = (TextView) findViewById(R.id.tv_nomeResp);
        tvEmailResp = (TextView) findViewById(R.id.tv_emailResp);
        tvTelResp = (TextView) findViewById(R.id.tv_telResp);
        tvLocal = (TextView) findViewById(R.id.tv_local);
        linearLayoutFeedback = (LinearLayout) findViewById(R.id.linearLayoutFeedback);
        linearLayoutRota = (LinearLayout) findViewById(R.id.linearLayoutRota);

        equipmentPrefs = new EquipmentPrefs(this);
        localPrefs = new LocalPrefs(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            equipment = (Equipment) getIntent().getSerializableExtra("Equipment"); //Obtaining data
            Responsible responsible = equipmentPrefs.getResponsible("" + equipment.idResp);
            final Local local = equipmentPrefs.getLocal("" + equipment.idLocal);

            tvNome.setText(equipment.nome);
            tvDescricao.setText(equipment.descricao);
            tvNomeResp.setText(responsible.nome);
            tvEmailResp.setText(responsible.email);
            tvTelResp.setText(responsible.telefone);
            tvLocal.setText(local.nome);

            File file = ImageStorage.getImage(getApplicationContext(), "" + equipment.id);

            String path = file.getAbsolutePath();
            if (path != null) {
                toolbarImage.setImageBitmap(BitmapFactory.decodeFile(path));
            }

            linearLayoutRota.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent();
                    Bundle bundle = new Bundle();

                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr=" + local.latitude + "," + local.longitude));
                    startActivity(intent);
                }
            });

            linearLayoutFeedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mailto = "mailto:agsf@fisica.ufc.br?&subject=[EMUFC-Equipamentos] " + equipment.nome;

                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                    emailIntent.setData(Uri.parse(mailto));

                    startActivity(Intent.createChooser(emailIntent, "Enviar e-mail"));
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String makeURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");

        if (sourcelat != 0) {
            urlString.append("?origin=");// from
            urlString.append(Double.toString(sourcelat));
            urlString.append(",");
            urlString.append(Double.toString(sourcelog));
        }
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=walking");
        urlString.append("&key=AIzaSyDOWOCIRsF3alHCKcnfdmQlZqmZrcfdIys");
        return urlString.toString();
    }
}
