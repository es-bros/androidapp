package br.ufc.es.emufc.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import br.ufc.es.emufc.R;

public class InitialActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    private int mSelectedItem;
    TextView mTitle;
    Fragment frag = null;

    SharedPreferences settings;
    public static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        settings = getSharedPreferences("Busca", Context.MODE_PRIVATE);
        editor = settings.edit();

        // Menu lateral
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCustom);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Mapa");

        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        selectFragment(bottomNavigationView.getMenu().getItem(0));

        bottomNavigationView.setSelectedItemId(R.id.action_mapa);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        selectFragment(item);
                        return true;
                    }
                });
    }

    private void selectFragment(MenuItem item) {
        frag = null;
        switch (item.getItemId()) {
            case R.id.action_mapa:
                frag = new MapaFragment();
                break;
            case R.id.action_busca:
                frag = new BuscaActivity();
                break;
            case R.id.action_noticias:
                frag = new NoticiasFragment();
                break;
        }

        updateToolbarText(item.getTitle());

        if (frag != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, frag).commit();
        }
    }

    private void updateToolbarText(CharSequence text) {
        Log.i("emufc", text + "");
        if (text.equals("Mapa")) {
            mTitle.setText("Mapa");
        } else if (text.equals("Buscar")) {
            mTitle.setText("Buscar Equipamentos");
        } else if (text.equals("Noticias")) {
            mTitle.setText("Novidades dos Equipamentos");
        } else {
            mTitle.setText("Emufc");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.contato:
                String mailto = "mailto:agsf@fisica.ufc.br?&subject=[EMUFC-Contato]";

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(mailto));

                startActivity(Intent.createChooser(emailIntent, "Enviar e-mail"));
                return true;
            case R.id.sobre:
                Intent intent = new Intent(this, SobreActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        editor.putString("etBusca", "");
        editor.commit();
    }
}
