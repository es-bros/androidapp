package br.ufc.es.emufc.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.ufc.es.emufc.R;
import br.ufc.es.emufc.dao.EquipmentPrefs;
import br.ufc.es.emufc.models.Equipment;
import br.ufc.es.emufc.models.Local;
import br.ufc.es.emufc.utils.EquipmentsAdapter;

public class LocaisActivity extends AppCompatActivity {

    TextView tvNome;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locais);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCustom);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Equipamentos Disponíveis");

        tvNome = (TextView) findViewById(R.id.tv_local);
        listView = (ListView) findViewById(R.id.lv_localEquip);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String idLocal = getIntent().getStringExtra("Local");
            Log.i("Emufc", "idlocal : " + idLocal);

            EquipmentPrefs equipmentPrefs = new EquipmentPrefs(this);
            List<Equipment> equipments = equipmentPrefs.getAllEquipmentsLocal(Integer.parseInt(idLocal));
            tvNome.setText(""+equipmentPrefs.getLocal(idLocal).nome);

            if (equipments != null) {
                EquipmentsAdapter equipmentsAdapter = new EquipmentsAdapter(getApplicationContext(), R.layout.custom_list2, equipments);
                listView.setAdapter(equipmentsAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Equipment equip = (Equipment) adapterView.getAdapter().getItem(i);

                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("Equipment", equip);
                        intent.putExtras(bundle);
                        intent.setClass(LocaisActivity.this, EquipmentActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
