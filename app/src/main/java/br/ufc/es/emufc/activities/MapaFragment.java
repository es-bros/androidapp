package br.ufc.es.emufc.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.ufc.es.emufc.R;
import br.ufc.es.emufc.dao.EquipmentPrefs;
import br.ufc.es.emufc.dao.LocalPrefs;
import br.ufc.es.emufc.dao.ResponsiblePrefs;
import br.ufc.es.emufc.models.Equipment;
import br.ufc.es.emufc.models.Local;
import br.ufc.es.emufc.models.Responsible;
import br.ufc.es.emufc.utils.EquipmentsAdapter;
import br.ufc.es.emufc.utils.ImageStorage;
import br.ufc.es.emufc.utils.JSONParser;
import br.ufc.es.emufc.utils.LocationCampis;
import br.ufc.es.emufc.utils.PropertyReader;

public class MapaFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private List<Local> listLocais = new ArrayList<>();
    private List<Responsible> listResponsibles = new ArrayList<>();
    private List<Equipment> listEquipments = new ArrayList<>();
    private final static String TAG = "EMUFC";
    private PropertyReader properties;

    SupportMapFragment mapFragment;
    String version = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.app_bar_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        //versão dos dados
        LocalPrefs localPrefs = new LocalPrefs(getActivity());
        version = localPrefs.settings.getString("version", "0");

        properties = new PropertyReader(getActivity());
        properties.getMyProperties("server.properties");
        mapFragment.getMapAsync(this);
    }

    private class TaskGetEquip extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Bundle extras = getActivity().getIntent().getExtras();
            if (extras != null) {
                readData();
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(String[] objects) {
            String response = "";
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(objects[0]);
            httpGet.setHeader("VERSION", version);

            if (isConnectedToServer(objects[0], Integer.parseInt(objects[1]))) {
                try {
                    HttpResponse execute = client.execute(httpGet);

                    if (execute.getStatusLine().getStatusCode() == 200) {
                        InputStream content = execute.getEntity().getContent();
                        BufferedReader buffer = new BufferedReader(new InputStreamReader(content));

                        String s = "";
                        while ((s = buffer.readLine()) != null) {
                            response += s;
                        }
                    } else {
                        response = "false";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                response = "false";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("false")) {
                readData();
            } else {
                JSONObject objResult = null;

                try {
                    objResult = new JSONObject(result);
                    version = String.valueOf(objResult.getJSONObject("databaseVersion").get("current")+"");

                    JSONArray objArray = objResult.getJSONArray("responsibles");

                    for (int i = 0; i < objArray.length(); i++) {
                        JSONObject obj = objArray.getJSONObject(i);

                        Responsible resp = new Responsible(
                                obj.getInt("id"),
                                obj.getString("name"),
                                obj.getString("email"),
                                obj.getString("phone"));

                        listResponsibles.add(resp);
                    }

                    objArray = objResult.getJSONArray("places");
                    for (int i = 0; i < objArray.length(); i++) {
                        JSONObject obj = objArray.getJSONObject(i);

                        Local local = new Local(
                                obj.getInt("id"),
                                obj.getString("name"),
                                obj.getDouble("latitude"),
                                obj.getDouble("longitude"));

                        listLocais.add(local);
                    }

                    objArray = objResult.getJSONArray("equipments");
                    for (int i = 0; i < objArray.length(); i++) {
                        JSONObject obj = objArray.getJSONObject(i);

                        Equipment equip = new Equipment(
                                obj.getInt("id"),
                                obj.getString("name"),
                                obj.getString("description"),
                                obj.getInt("place_id"),
                                obj.getInt("responsible_id"));

                        listEquipments.add(equip);

                        new GetImages("http://" +
                                properties.getProperty("server.ip") + "/" +
                                properties.getProperty("server.route.images") + "/" +
                                equip.id + ".jpeg", "" + equip.id).execute();
                    }

                    saveData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(15.0f);
        mMap.setMaxZoomPreference(19.0f);

        if (mMap != null) {
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getActivity().getLayoutInflater().inflate(R.layout.custom_marker, null);

                    TextView nome = (TextView) v.findViewById(R.id.tv_nome);
                    TextView mais = (TextView) v.findViewById(R.id.tvMais);
                    final ListView listView = (ListView) v.findViewById(R.id.lv_equipments);

                    nome.setText(marker.getTitle());

                    List<Equipment> equipments = new ArrayList<Equipment>(listEquipments(marker.getSnippet()));

                    if (equipments != null) {
                        if (equipments.size() > 2) {
                            mais.setVisibility(View.VISIBLE);
                            Equipment equip1 = equipments.get(0);
                            Equipment equip2 = equipments.get(1);
                            equipments.clear();
                            equipments.add(equip1);
                            equipments.add(equip2);
                        }
                        EquipmentsAdapter equipmentsAdapter = new EquipmentsAdapter(getActivity(), R.layout.custom_list, equipments);
                        listView.setAdapter(equipmentsAdapter);
                    }

                    return v;
                }
            });
        }
        //clearData();

        // url - timeout
        new TaskGetEquip().execute("http://" +
                        properties.getProperty("server.ip") + ":" +
                        properties.getProperty("server.port") + "/" +
                        properties.getProperty("server.route.equipments"),
                properties.getProperty("server.timeout"));

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        final LocationCampis locationCampis = new LocationCampis();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(locationCampis.findCampi(getLocation()))      // Sets the center of the map to Mountain View
                .zoom(18.0f)                   // Sets the zoom
                .bearing(300)                // Sets the orientation of the camera to east
                .build();                   // Creates a CameraPosition from the builder

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            new ConnectAsyncTask((String) getActivity().getIntent().getStringExtra("Localizacao")).execute(); //Obtaining data
        }
    }

    public LatLng getLocation() {
        // Get the location manager
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        Location location = locationManager.getLastKnownLocation(bestProvider);
        Double lat, lon;
        try {
            lat = location.getLatitude();
            lon = location.getLongitude();
            return new LatLng(lat, lon);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        // Retrieve the data from the marker.
        Integer idMarker = (Integer) marker.getTag();
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent i = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("Local", marker.getSnippet());
        Log.i("Emufc", "idlocal : " + marker.getSnippet());

        i.putExtras(bundle);
        i.setClass(getActivity(), LocaisActivity.class);
        startActivity(i);
    }

    private void markerMap(Local local) {
        LatLng position = new LatLng(local.latitude, local.longitude);

        mMap.addMarker(new MarkerOptions()
                .position(position)
                .title(local.nome)
                .snippet(String.valueOf(local.id))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)))
                .setTag(local.id);
    }

    public boolean isConnectedToServer(String url, int timeout) {
        try {
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(timeout);
            connection.connect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Equipment> listEquipments(String id) {
        List<Equipment> list = new ArrayList<>();

        for (int i = 0; i < listEquipments.size(); i++) {
            if (listEquipments.get(i).idLocal == Integer.parseInt(id)) {
                list.add(listEquipments.get(i));
            }
        }

        return list;
    }

    public void readData() {
        Gson gson = new Gson();
        EquipmentPrefs equipEditor = new EquipmentPrefs(getActivity());

        Map<String, String> map = (Map<String, String>) equipEditor.settings.getAll();

        if (!map.isEmpty()) {
            for (String result : map.keySet()) {
                Equipment equipment = gson.fromJson(map.get(result), Equipment.class);
                markerMap(equipEditor.getLocal("" + equipment.idLocal));
                listEquipments.add(equipment);
            }
        }
    }

    public void saveData() {
        clearData();
        Gson gson = new Gson();
        EquipmentPrefs equipEditor = new EquipmentPrefs(getActivity());
        LocalPrefs localEditor = new LocalPrefs(getActivity());
        ResponsiblePrefs respEditor = new ResponsiblePrefs(getActivity());

        for (Equipment equipment : listEquipments) {
            equipEditor.editor.putString(String.valueOf(equipment.id), gson.toJson(equipment));
        }

        for (Responsible responsible : listResponsibles) {
            respEditor.editor.putString(String.valueOf(responsible.id), gson.toJson(responsible));
        }

        for (Local local : listLocais) {
            localEditor.editor.putString(String.valueOf(local.id), gson.toJson(local));
            markerMap(local);
        }

        localEditor.editor.putString("version", version); //versão dos dados

        localEditor.editor.commit();
        equipEditor.editor.commit();
        respEditor.editor.commit();
    }

    public void clearData() {
        EquipmentPrefs equipEditor = new EquipmentPrefs(getActivity());
        LocalPrefs localEditor = new LocalPrefs(getActivity());
        ResponsiblePrefs respEditor = new ResponsiblePrefs(getActivity());

        equipEditor.editor.clear();
        localEditor.editor.clear();
        respEditor.editor.clear();

        localEditor.editor.commit();
        equipEditor.editor.commit();
        respEditor.editor.commit();
    }

    private class GetImages extends AsyncTask<Object, Object, Object> {

        private String requestUrl, imagename_;
        private FileOutputStream fos;

        private GetImages(String requestUrl, String _imagename_) {
            this.requestUrl = requestUrl;
            this.imagename_ = _imagename_;
        }

        @Override
        protected Object doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                return BitmapFactory.decodeStream(conn.getInputStream());

            } catch (Exception ex) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                ImageStorage.saveToInternalStorage(getContext(), (Bitmap) o, imagename_);
            }
        }
    }

    private class ConnectAsyncTask extends AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        String url;

        ConnectAsyncTask(String urlPass) {
            url = urlPass;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Por favor, espere...");
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            JSONParser jParser = new JSONParser();
            String json = jParser.getJSONFromUrl(url);
            return json;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null) {
                drawPath(result);
            }
        }
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public void drawPath(String result) {

        try {
            //Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            Log.i("EMUFC", routeArray.toString());
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(12)
                    .color(Color.parseColor("#05b1fb"))//Google maps blue color
                    .geodesic(true)
            );
        } catch (JSONException e) {

        }
    }
}
