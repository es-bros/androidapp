package br.ufc.es.emufc.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import br.ufc.es.emufc.R;

public class SobreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCustom);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Sobre");

        JustifiedTextView jtvSobre = (JustifiedTextView) findViewById(R.id.jtv_sobre);
        jtvSobre.setText("O EMUFC é um aplicativo que mapeia os equipamentos multiusuários da UFC. " +
                "\n\nOs marcadores no mapa mostram os locais onde há os equipamentos estão instalados." +
                "\n\nAs funcionalidades do aplicativo são: ver as informações detalhadas dos equipamentos " +
                "(nome e descrição do equipamento, local onde está instalado, nome, e-mail e telefone do responsável); " +
                "buscar equipamentos pelo nome ou descrição; e permitir traçar rotas entre o pesquisador e o equipamento desejado.");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
