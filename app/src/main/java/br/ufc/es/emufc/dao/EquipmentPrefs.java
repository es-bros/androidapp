package br.ufc.es.emufc.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.ufc.es.emufc.models.Equipment;
import br.ufc.es.emufc.models.Local;
import br.ufc.es.emufc.models.Responsible;

/**
 * Created by anderson on 25/05/17.
 */
public class EquipmentPrefs {

    /** This application's preferences label */
    public static final String PREFS_NAME = "br.ufc.es.emufc.dao.EquipmentPrefs";

    /** This application's preferences */
    public static SharedPreferences settings;

    /** This application's settings editor*/
    public static SharedPreferences.Editor editor;

    /** Constructor takes an android.content.Context argument*/
    public Context context;

    public EquipmentPrefs(Context ctx){
        context = ctx;

        if(settings == null){
            settings = context.getSharedPreferences(PREFS_NAME,
                    Context.MODE_PRIVATE );
        }
       /*
        * Get a SharedPreferences editor instance.
        * SharedPreferences ensures that updates are atomic
        * and non-concurrent
        */
        editor = settings.edit();
    }

    public Local getLocal(String id) {
        LocalPrefs localPrefs = new LocalPrefs(context);
        return new Gson().fromJson(localPrefs.settings.getString(id, ""), Local.class);
    }

    public Responsible getResponsible(String id) {
        ResponsiblePrefs responsiblePrefs = new ResponsiblePrefs(context);
        return new Gson().fromJson(responsiblePrefs.settings.getString(id, ""), Responsible.class);
    }

    public List<Equipment> getAllEquipments() {
        List<Equipment> list = new ArrayList<>();
        Map<String, String> map = (Map<String, String>) settings.getAll();

        if (!map.isEmpty()) {
            for (String result : map.keySet()) {
                list.add(new Gson().fromJson(map.get(result), Equipment.class));
            }
        }

        return list;
    }

    public List<Equipment> getAllEquipmentsLocal(int id) {
        LocalPrefs localPrefs = new LocalPrefs(context);
        List<Equipment> list = new ArrayList<>();
        List<Equipment> listEquip = getAllEquipments();

        for(int i = 0; i < listEquip.size(); i++) {
            if(listEquip.get(i).idLocal == id) {
                list.add(listEquip.get(i));
            }
        }

        return list;
    }
}