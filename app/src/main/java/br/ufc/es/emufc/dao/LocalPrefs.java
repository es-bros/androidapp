package br.ufc.es.emufc.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.ufc.es.emufc.models.Equipment;
import br.ufc.es.emufc.models.Local;

/**
 * Created by anderson on 25/05/17.
 */
public class LocalPrefs {

    /**
     * This application's preferences label
     */
    public static final String PREFS_NAME = "br.ufc.es.emufc.dao.LocalPrefs";

    /**
     * This application's preferences
     */
    public static SharedPreferences settings;

    /**
     * This application's settings editor
     */
    public static SharedPreferences.Editor editor;

    /**
     * Constructor takes an android.content.Context argument
     */
    public LocalPrefs(Context ctx) {
        if (settings == null) {
            settings = ctx.getSharedPreferences(PREFS_NAME,
                    Context.MODE_PRIVATE);
        }
       /*
        * Get a SharedPreferences editor instance.
        * SharedPreferences ensures that updates are atomic
        * and non-concurrent
        */
        editor = settings.edit();
    }
}