package br.ufc.es.emufc.dao;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by anderson on 25/05/17.
 */
public class ResponsiblePrefs {

    /** This application's preferences label */
    public static final String PREFS_NAME = "br.ufc.es.emufc.dao.ResponsiblePrefs";

    /** This application's preferences */
    public static SharedPreferences settings;

    /** This application's settings editor*/
    public static SharedPreferences.Editor editor;

    /** Constructor takes an android.content.Context argument*/
    public ResponsiblePrefs(Context ctx){
        if(settings == null){
            settings = ctx.getSharedPreferences(PREFS_NAME,
                    Context.MODE_PRIVATE );
        }
       /*
        * Get a SharedPreferences editor instance.
        * SharedPreferences ensures that updates are atomic
        * and non-concurrent
        */
        editor = settings.edit();
    }
}