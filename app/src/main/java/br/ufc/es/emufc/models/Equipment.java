package br.ufc.es.emufc.models;

import java.io.Serializable;

/**
 * Created by anderson on 14/05/17.
 */
public class Equipment implements Serializable {

    public int id = -1;
    public String nome = "";
    public String descricao = "";
    public int idLocal = -1;
    public int idResp = -1;

    public Equipment(int id,
                     String nome,
                     String descricao,
                     int idLocal,
                     int idResp) {

        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.idLocal = idLocal;
        this.idResp = idResp;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", idLocal=" + idLocal +
                ", idResp=" + idResp +
                '}';
    }
}
