package br.ufc.es.emufc.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anderson on 21/05/17.
 */
public class Local implements Serializable {

    public int id = -1;
    public String nome = "";
    public double latitude = -1;
    public double longitude = -1;

    public Local(int id, String nome, double lat, double longitude) {
        this.id = id;
        this.nome = nome;
        this.latitude = lat;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Local{" +
                "nome='" + nome + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
