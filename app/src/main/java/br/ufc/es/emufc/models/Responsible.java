package br.ufc.es.emufc.models;

import java.io.Serializable;

/**
 * Created by anderson on 16/06/17.
 */
public class Responsible implements Serializable {

    public int id = -1;
    public String nome = "";
    public String email = "";
    public String telefone = "";

    public Responsible(int id, String nome, String email, String telefone) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Responsible{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", telefone='" + telefone + '\'' +
                '}';
    }
}
