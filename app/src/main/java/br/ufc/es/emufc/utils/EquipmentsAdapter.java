package br.ufc.es.emufc.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.ufc.es.emufc.R;
import br.ufc.es.emufc.models.Equipment;

/**
 * Created by anderson on 21/05/17.
 */
public class EquipmentsAdapter extends ArrayAdapter<Equipment> {

    private int layoutResource;
    private Context context;

    public EquipmentsAdapter(Context context, int layoutResource, List<Equipment> equipments) {
        super(context, layoutResource, equipments);
        this.context = context;
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        Equipment equipment = getItem(position);

        if (equipment != null) {
            TextView nameEquipment = (TextView) view.findViewById(R.id.nameEquipment);

            if (nameEquipment != null) {
                nameEquipment.setText(equipment.nome);
            }
        }

        return view;
    }
}
