package br.ufc.es.emufc.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anderson on 02/06/17.
 */
public class LocationCampis {

    //Pici
    LatLng ponto1Pici = new LatLng(-3.7463867, -38.5827829);
    LatLng ponto2longPici = new LatLng(-3.7490742, -38.5697226);

    //Porangabussu
    LatLng ponto1Porangabussu = new LatLng(-3.7472223, -38.5558268);
    LatLng ponto2Porangabussu = new LatLng(-3.7483692, -38.5520867);

    List<LatLngBounds> listBounds;

    public LocationCampis() {
        listBounds = new ArrayList<>();

        listBounds.add(LatLngBounds.builder().include(ponto1Pici).include(ponto2longPici).build());
        listBounds.add(LatLngBounds.builder().include(ponto1Porangabussu).include(ponto2Porangabussu).build());
    }

    public LatLng findCampi(LatLng position) {
        if(position != null) {
            for (LatLngBounds campi : listBounds) {
                if (campi.contains(position)) {
                    return campi.getCenter();
                }
            }
        }

        return listBounds.get(0).getCenter();
    }
}
